import React from "react";
import ReactDOM from "react-dom";
import { Provider } from "react-redux";
import { createStore, applyMiddleware } from "redux";
import { Router, Route, browserHistory } from "react-router";
import App from "./components/app";
import reducers from "./reducers";
import Customers from "./components/customers";
import UserList from "./components/user_list";
import Auth from "./components/require_auth";
import Async from "./middlewares/async";
const createStoreWithMiddleware = applyMiddleware(Async)(createStore);

ReactDOM.render(
  <Provider store={createStoreWithMiddleware(reducers)}>
    <Router history={browserHistory}>
      <Route path="/" component={App}>
        <Route path="/customers" component={Auth(Customers)} />
        <Route path="/users" component={Auth(UserList)} />
      </Route>
    </Router>
  </Provider>,
  document.querySelector(".container")
);
